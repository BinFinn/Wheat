<?php
// 根据.env或.env.example文件中APP_ENV值切换不同数据库

if(env('APP_ENV', '') == 'local') {
	return [
		// required
	    'database_type' => env('DB_CONNECTION', 'mysql'),
	    'database_name' => env('DB_DATABASE', ''),
	    'server' => env('DB_HOST', ''),
	    'username' => env('DB_USERNAME', ''),
	    'password' => env('DB_PASSWORD', ''),
		// // [optional]
	    'charset' => 'utf8',
		// 'charset' => 'utf8mb4',
		// 'collation' => 'utf8mb4_general_ci',
		'port' => 3306,
	];
} else if (env('APP_ENV', '') == 'product') {
	return [
		// required
	    'database_type' => env('DB_SERVER_CONNECTION', 'mysql'),
	    'database_name' => env('DB_SERVER_DATABASE', ''),
	    'server' => env('DB_SERVER_HOST', ''),
	    'username' => env('DB_SERVER_USERNAME', ''),
	    'password' => env('DB_SERVER_PASSWORD', ''),
		// // [optional]
	    'charset' => 'utf8',
		// 'charset' => 'utf8mb4',
		// 'collation' => 'utf8mb4_general_ci',
		'port' => 3306,

		// // [optional] Table prefix
		// 'prefix' => 'PREFIX_',
		// // [optional] Enable logging (Logging is disabled by default for better performance)
		// 'logging' => true,
		// // [optional] MySQL socket (shouldn't be used with server and port)
		// 'socket' => '/tmp/mysql.sock',
		// // [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
		// 'option' => [
		// 	PDO::ATTR_CASE => PDO::CASE_NATURAL
		// ],
		// // [optional] Medoo will execute those commands after connected to the database for initialization
		// 'command' => [
		// 	'SET SQL_MODE=ANSI_QUOTES'
		// ]
	];
} else {
	$err = [
		'ERR:'=> 'Warming',
		'FILE:' => __FILE__,
		'LINE:' => __LINE__
	];
	dump($err);
	exit();
}

