<?php
/**
 * 我不管，我是最好的PHP开发框架。
 */

// Autoload 自动载入
require '../vendor/autoload.php';

// 加载 Whoops 错误提示
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

// 加载环境变量
$dotenv = Dotenv\Dotenv::create(__DIR__ . '/../', '.env'); 
$dotenv->overload();// load()当需要替换已定义的值时可以用overload()

// 路由配置
require '../routes/web.php';


// Autoload 自动载入
// require '../vendor/autoload.php';
// 路由配置
// require '../config/routes.php';
