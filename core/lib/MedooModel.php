<?php
namespace core\lib;

use Medoo\Medoo;

class MedooModel extends Medoo
{

    public function __construct()
    {
        $database = include __DIR__ . '/../../config/database.php';
     	// return parent::__construct($database);

        // Initialize
		// return new Medoo($database);
		try {
			parent::__construct($database);
		} catch (\PDOException $e) {
			dump($e->getMessage());
		}
    }
}