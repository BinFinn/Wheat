<?php
/**
 * ArticleController
 */
namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Test;

class ArticleController extends BaseController
{
    public function index() {
		// 命名空间的方式，先use后调用。
        // $article  = Test::first();
        // echo json_encode($article , true);

		// 定义psr-4自动加载目录，即可直接使用下面方式
        $artModel = new \App\Models\Test();
		$article  = $artModel::first();
		// 简单的视图加载
		require dirname(__FILE__) . '/../views/article/list.php';
        //echo json_encode($article , true);
    }

    /**
     * [usage description]
     * @return [type] [description]
     */
    public function usage() {
        $model = new \App\Models\Article();
        // dump($model);
        $ret = $model->lists();
        $data = array(
            'name' => 'fdf',
            'content' => 'ss'
        );
        // $ret = $model->getOne(1);
        // $ret = $model->setOne(4, $data); // 修改成功为1,不成功为0
        // $ret = $model->del(5); // 执行成功为1，不成功为0
        dump($ret);
    }
}