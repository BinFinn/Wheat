<?php
/**
 * HomeController
 */
namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Article;

class HomeController extends BaseController
{
    public function index() {
		// 命名空间的方式，先use后调用。
        // $article  = Article::first();
        // echo json_encode($article , true);

		// 定义psr-4自动加载目录，即可直接使用下面方式
        $artModel = new \App\Models\Article();
		$article  = $artModel::first();
        echo json_encode($article , true);
    }
}