<?php
/**
 * Article
 */

namespace App\Models;

use App\Models\BaseModel;
// use Core\Lib\MedooModel;

 
// class Article extends MedooModel
class Article extends BaseModel
{
    private $table = 'articles';

    public function __construct() {
        parent::__construct();
        $this->setTable($this->table);
    }

    /// 下面可以编写业务代码了！

    
 
}