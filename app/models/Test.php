<?php
/**
* Test Model
*/
namespace App\Models;

class Test
{
    public static function first() {
	    // 假定数据库用户名：root，密码：123456，数据库：RUNOOB 
        $con = mysqli_connect("localhost", "root", "123456", "db_study"); 
        if (mysqli_connect_errno($con)) { 
            echo "连接 MySQL 失败: " . mysqli_connect_error(); 
        }
        mysqli_set_charset($con, "UTF8");
        // 执行查询
        $result = mysqli_query($con, "SELECT * FROM articles");
        
		$data = [];
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			$data[] = $row;
        }
        mysqli_close($con);
		return $data;
    }
}