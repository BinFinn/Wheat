<?php
/**
 * BaseModel 基本模型类
 */

namespace App\Models;
use Core\Lib\MedooModel;
 
class BaseModel extends MedooModel
{
    private $table = '';

	/**
	 * 初始化模型类 (一般用于控制器直接new时调用)
	 * @param string $table [description]
	 */
    public function __construct($table = '')
    {
		parent::__construct();
    	if (empty($table)) {
    		// dump('表名不能为空');
    	} else {
    		$this->table = $table;
    	}
    }

	/**
	 * 设置模型类 (用于模型类use后定向设置)
	 * @param string $table [description]
	 */
    public function setTable($table = '')
    {
    	if (empty($table)) {
    		// dump('表名不能为空');
    	} else {
    		$this->table = $table;
    	}
    }
 
    /**
     * 查询字段
     * @return array
     */
    public function lists()
    {
        $ret = $this->select($this->table, '*');
        return $ret;
    }
    /**
     * 查询单条
     */
    public function getOne($id)
    {
        $ret =  $this->get($this->table, '*', array('id'=>$id));
        return $ret;
    }
    /**
     * 修改
     */
    public function setOne($id ,$data)
    {
        $ret = $this->update($this->table, $data, array('id'=>$id));
        return $ret;
    }
    /**
     * 删除
     */
    public function del($id)
    {
        $ret = $this->delete($this->table, array('id'=>$id));
        return $ret;
    }
 
}