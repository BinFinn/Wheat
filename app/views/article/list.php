<!DOCTYPE html>
<html>
<head>
  <title>view视图调用测试</title>
</head>
<style type="text/css">
  table, tr, th, td {
    border: 1px solid #000;
    border-collapse:collapse;
  }
</style>
<body>
  <table>
      <tr>
        <th>ID</th>
        <th>标题</th>
        <th>内容</th>
      </tr>
      <?php foreach($article as $k => $v) { ?>
      <tr>
        <td><?php echo $v['id']; ?></td>
        <td><?php echo $v['title']; ?></td>
        <td><?php echo $v['content']; ?></td>
      </tr>
      <?php } ?>
  </table>
</body>
</html>