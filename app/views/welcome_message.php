<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Wheat</title>
    <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="assets/favicon.png">
	<style type="text/css">
	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }
	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}
	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}
	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}
	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}
	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>
<div id="container">
	<h1><img src="assets/wheat.svg" width="64px" style="vertical-align: middle;">Welcome to <?php echo env("APP_NAME", "Wheat"); ?>!</h1>
	<div id="body">
		<p>The page you are looking at is being generated dynamically by <?php echo env("APP_NAME", "Wheat"); ?>.</p>

		<p>If you would like to edit this page you'll find it located at:</p>
		<code>app/views/welcome_message.html</code>

		<p>The corresponding controller for this page is found at:</p>
		<code>routes/web.php</code>

		<p>If you are exploring <?php echo env("APP_NAME", "Wheat"); ?> for the very first time, you should start by reading the <a href="https://gitee.com/BinFinn/Wheat">User Guide</a>.</p>
	</div>
	<p class="footer"><?php echo "Wheat Version:" . env("APP_VERSION", "1.0.0"); ?></p>
</div>

</body>
</html>