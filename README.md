# 麦子框架Wheat Framework©

#### 介绍
```
一个完美支持 Composer 包管理的框架，我称她Wheat麦子框架。希望她总能给你带去“丰收”的喜悦。
```

![](https://images.gitee.com/uploads/images/2019/1009/005226_9b8db636_1402928.png)

#### 软件架构

```
 - PSR-0 (Autoloading Standard) 自动加载标准
 - PSR-1 (Basic Coding Standard) 基础编码标准
 - PSR-2 (Coding Style Guide) 编码风格向导
 - PSR-3 (Logger Interface) 日志接口
 - PSR-4 (Improved Autoloading) 自动加载优化标准
```


#### 安装教程

```
1. git clone git@gitee.com:BinFinn/Wheat.git
2. cd Wheat && composer install
3. run QuickStart.bat
```

#### 框架目录结构

```
Wheat
├─app
│  ├─controllers
│  ├─models
│  ├─views
│  └─modules
│  └─Views
├─config
├─core
│  └─lib
├─helper
├─public
├─routes
└─vendor
```

#### Basic Package List

```
composer require noahbuscher/macaw // 路由
composer require vlucas/phpdotenv // 环境变量
composer require symfony/var-dumper // PHP调试函数
composer require filp/whoops // 错误提示
composer require catfan/medoo // 数据库操作
```

`站在巨人的肩膀上总能使我们看到最远，非常感谢默默奉献的人`

#### Helper

```
/helper/main.php  // 系统主要函数
/helper/helper.php // 帮助类函数
/helper/function.php // 公共的处理函数
/app/modules/function.php //当前模块的函数
```

#### 使用说明

`转载注明来源。`

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

6. https://gitee.com/gitee-stars/)