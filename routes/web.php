<?php
/**
 * 路由文件
 */
use NoahBuscher\Macaw\Macaw as Route;

// Route::get('/article/index', 'App\Controllers\HomeController@index');

Route::get('/article/index', 'App\Controllers\ArticleController@index');

Route::get('/article/usage', 'App\Controllers\ArticleController@usage');

Route::get('/first', function () {
	echo '123';
});

Route::get('/second/(:any)', function ($id) {
	echo $id;
});
// (:any) 和 (:all) 都可以匹配所有
Route::get('/second/(:any)/(:all)', function ($id, $name) {
	echo $id . '-' . $name;
	exit();
});

Route::post('/', function() {
  echo 'I’m a POST request!';
});

Route::get('/', function() {
  include '../app/views/welcome_message.php';
});

// // 未匹配路由会影响路由后匹配参数，如果未匹配直接error处理返回404。
// Route::get('(:all)', function($route) {。
//   echo 'ERR: 未匹配到路由' . $route;
// });

Route::error(function() {
  echo '404 :: Not Found';
});

Route::dispatch();